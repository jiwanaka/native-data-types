extern void doInt();
extern void flowInt();

extern void doSignedInt();
extern void flowSignedInt();

extern void doUnsignedInt();
extern void flowUnsignedInt();