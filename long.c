#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "long.h"

///////////////////////////////////////////////////////////////////////////////
/// long

/// Print the characteristics of the "long" datatype
void doLong() {
   printf(TABLE_FORMAT_LONG, "long", CHAR_BIT, CHAR_BIT/8, LONG_MIN, LONG_MAX);
}


/// Print the overflow/underflow characteristics of the "long" datatype
void flowLong() {
   long overflow = LONG_MAX;
   printf("long overflow:  %ld + 1 ", overflow++);
   printf("becomes %ld\n", overflow);

   long underflow = LONG_MIN;
   printf("long underflow:  %ld - 1 ", underflow--);
   printf("becomes %ld\n", underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// signed long

/// Print the characteristics of the "signed long" datatype
void doSignedLong() {
   printf(TABLE_FORMAT_LONG, "signed long", CHAR_BIT, CHAR_BIT/8, LONG_MIN, LONG_MAX);
}


/// Print the overflow/underflow characteristics of the "signed long" datatype
void flowSignedLong() {
   signed long overflow = LONG_MAX;
   printf("signed long overflow:  %ld + 1 ", overflow++);
   printf("becomes %ld\n", overflow);

   signed long underflow = LONG_MIN;
   printf("signed long underflow:  %ld - 1 ", underflow--);
   printf("becomes %ld\n", underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// unsigned long

/// Print the characteristics of the "unsigned long" datatype
void doUnsignedLong() {
   printf(TABLE_FORMAT_ULONG, "unsigned long", CHAR_BIT, CHAR_BIT/8, 0, ULONG_MAX);  
}

/// Print the overflow/underflow characteristics of the "unsigned long" datatype
void flowUnsignedLong() {
   unsigned long overflow = ULONG_MAX;
   printf("unsigned long overflow:  %llu + 1 ", overflow++);
   printf("becomes %llu\n", overflow);

   unsigned long underflow = 0;  
   printf("unsigned long underflow:  %llu - 1 ", underflow--);
   printf("becomes %llu\n", underflow);
}