#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "int.h"

///////////////////////////////////////////////////////////////////////////////
/// int

/// Print the characteristics of the "int" datatype
void doInt() {
   printf(TABLE_FORMAT_INT, "int", CHAR_BIT, CHAR_BIT/8, INT_MIN, INT_MAX);
}


/// Print the overflow/underflow characteristics of the "int" datatype
void flowInt() {
   int overflow = INT_MAX;
   printf("int overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   int underflow = INT_MIN;
   printf("int underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// signed int

/// Print the characteristics of the "signed int" datatype
void doSignedInt() {
   printf(TABLE_FORMAT_INT, "signed int", CHAR_BIT, CHAR_BIT/8, INT_MIN, INT_MAX);
}


/// Print the overflow/underflow characteristics of the "signed int" datatype
void flowSignedInt() {
   signed int overflow = INT_MAX;
   printf("signed int overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   signed int underflow = INT_MIN;
   printf("signed int underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// unsigned int

/// Print the characteristics of the "unsigned int" datatype
void doUnsignedInt() {
   printf(TABLE_FORMAT_UINT, "unsigned int", CHAR_BIT, CHAR_BIT/8, 0, UINT_MAX);  
}

/// Print the overflow/underflow characteristics of the "unsigned int" datatype
void flowUnsignedInt() {
   unsigned int overflow = UINT_MAX;
   printf("unsigned int overflow:  %llu + 1 ", overflow++);
   printf("becomes %llu\n", overflow);

   unsigned int underflow = 0;  
   printf("unsigned int underflow:  %llu - 1 ", underflow--);
   printf("becomes %llu\n", underflow);
}